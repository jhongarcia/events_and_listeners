<?php

namespace App\Http\Controllers;

use App\Events\PrimeNumberEvent;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class PrimeNumberController
 *
 * @package App\Http\Controllers
 */
class PrimeNumberController extends Controller
{
    /**
     * Calcular n veces los números primos
     *
     * @param int $iterations Contiene la cantidad de iteraciones a ejecutar
     * @return JsonResponse Contiene el mensaje de respuesta
     * @author Jhon García
     */
    public function calculate($iterations)
    {
        $length = 1000000;

        // Iterar de 0 a $length para generar los eventos
        for ($i = 0; $i < $iterations; $i++){
            // Generar evento
            event(new PrimeNumberEvent($length));
        }

        // Retornar respuesta al usuario
        return response()->json([
            'message' => 'Successfully generated events'
        ], 200);
    }
}
