<?php

namespace App\Listeners;

use App\Mail\PrimeNumberMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class PrimeNumberEmailListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $mail = new PrimeNumberMail($event->length);
        Mail::to('jhonfgarciac1@gmail.com')->send($mail);

        Log::info('Correo enviado');
    }
}
