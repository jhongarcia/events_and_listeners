<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class PrimeNumberCalculateListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $contador = 0;
        $arreglo = array($event->length + 1);

        $arreglo[0] = false;
        $arreglo[1] = false;

        for($i = 2; $i <= $event->length; $i++){
            $arreglo[$i] = null;
        }

        foreach($arreglo as $key => $numero){
            if (is_null($arreglo[$key])){
                $arreglo[$key] = true;
                for($i = $key * 2; $i <= $event->length; $i += $key){
                    if (is_null($arreglo[$i])){
                        $arreglo[$i] = false;
                    }
                }
            }
        }

        for($i = 0; $i<count($arreglo); $i++){
            if($arreglo[$i]) $contador++;
        }

        Log::info("Existen " . $contador . " números primos en el arreglo.");
    }
}
