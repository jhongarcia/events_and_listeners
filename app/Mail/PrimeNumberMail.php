<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PrimeNumberMail extends Mailable
{
    use Queueable, SerializesModels;

    public $length;

    /**
     * Create a new message instance.
     *
     * @param $length
     */
    public function __construct($length)
    {
        $this->length = $length;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Números primos')
            ->from('jhonfgarciac1@gmail.com', 'Pruebas')
            ->view('mail.prime_numbers');
    }
}
